import { useState, useEffect } from "react";
import { useParams } from "react-router";
import { useSelector } from "react-redux";

function AcceptOrDeclineBooking() {
  const { id, status } = useParams();

  const login = useSelector((s) => s.login);
  useEffect(() => {
    const handleBooking = async () => {
      const res = await fetch(
        `http://localhost:9999/api/v1/booking/${status}/${id}`,
        {
          headers: { Authorization: login.token },
          method: "PUT",
        }
      );
    };
    handleBooking();
  }, [id, login.token, status]);

  return (
    <div>
      {status === "accept" && <label>Has aceptado la reserva</label>}
      {status === "decline" && <label>Has rechazado la reserva</label>}
    </div>
  );
}

export default AcceptOrDeclineBooking;
