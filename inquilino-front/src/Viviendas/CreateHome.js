import React from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

function CreateHome() {
  const [ciudad, setCiudad] = useState("");
  const [provincia, setProvincia] = useState("");
  const [direccion, setDireccion] = useState("");
  const [nHabitaciones, setNHabitaciones] = useState("");
  const [m2, setM2] = useState("");
  const [nBanos, setNBanos] = useState("");
  const [precio_piso, setPrecio_piso] = useState("");

  const [descripcion, setDescripcion] = useState("");

  const [error, setError] = useState();

  const login = useSelector((s) => s.login);

  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const pisoImagen = e.target.pisoImagen.files[0];

    const fd = new FormData();
    fd.append("imagen", pisoImagen);
    fd.append("ciudad", ciudad);
    fd.append("provincia", provincia);
    fd.append("direccion", direccion);
    fd.append("nHabitaciones", nHabitaciones);
    fd.append("m2", m2);
    fd.append("nBanos", nBanos);
    fd.append("precio_piso", precio_piso);

    fd.append("descripcion", descripcion ? descripcion : []);

    const res = await fetch("http://localhost:9999/api/v1/pisos/", {
      headers: { Authorization: login.token },
      body: fd,
      method: "POST",
    });
    if (res.ok) {
      history.push(`/user/${login.id}/Viviendas`);
    } else {
      console.log("Error");
      setError(true);
    }
  };

  const [preview, setPreview] = useState(null);

  const handlePreview = (e) => {
    e.preventDefault();
    setPreview(URL.createObjectURL(e.target.files[0]));
  };

  const hiddenUpload = React.useRef(null);

  const handleUpload = (e) => {
    e.preventDefault();
    hiddenUpload.current.click();
  };

  return (
    <div className="createHome">
      <h1>Alta de casa</h1>
      <div className="createHomeContainer">
        <div className="createHomeForm">
          <h1>Rellena los datos</h1>
          <form onSubmit={handleSubmit}>
            <label>
              <div className="previewContainer">
                <span
                  style={{
                    marginBottom: "10px",
                  }}
                >
                  Foto seleccionada:
                </span>
                {preview && (
                  <div
                    className="previewCreate"
                    style={{ backgroundImage: `url(${preview})` }}
                  />
                )}
              </div>
              <div>
                <input
                  name="pisoImagen"
                  type="file"
                  onChange={handlePreview}
                  ref={hiddenUpload}
                  style={{ display: "none" }}
                  accept="image/*"
                />
                <button onClick={handleUpload}>Sube una foto</button>
              </div>
            </label>
            <input
              name="ciudad"
              type="text"
              placeholder="Ciudad..."
              value={ciudad}
              onChange={(e) => setCiudad(e.target.value)}
            />
            <input
              name="provincia"
              type="text"
              placeholder="Provincia..."
              value={provincia}
              onChange={(e) => setProvincia(e.target.value)}
            />
            <input
              name="direccion"
              type="text"
              placeholder="Direccion..."
              value={direccion}
              onChange={(e) => setDireccion(e.target.value)}
            />
            <input
              name="banos"
              type="number"
              placeholder="Baños..."
              value={nBanos}
              onChange={(e) => setNBanos(e.target.value)}
            />
            <input
              name="habitaciones"
              type="number"
              placeholder="Habitaciones..."
              value={nHabitaciones}
              onChange={(e) => setNHabitaciones(e.target.value)}
            />
            <input
              name="m2"
              type="number"
              placeholder="m2..."
              value={m2}
              onChange={(e) => setM2(e.target.value)}
            />
            <input
              name="precio"
              type="number"
              placeholder="Precio..."
              value={precio_piso}
              onChange={(e) => setPrecio_piso(e.target.value)}
            />

            <textarea
              style={{
                marginTop: "40px",
              }}
              name="descripcion"
              rows="4"
              cols="45"
              placeholder="Descripción general"
              value={descripcion}
              onChange={(e) => setDescripcion(e.target.value)}
            />
            {error && <div className="errorCreacion">Error en la creación</div>}
            <button className="publicarHome">Publicar</button>
          </form>
        </div>
      </div>
      <button
        style={{
          marginTop: "15px",
          padding: "10px",
          borderRadius: "8px",
          boxShadow: "1px 2px 3px 2px rgba(150, 150, 150, 0.75)",
        }}
      >
        <Link to="/">Volver</Link>
      </button>
    </div>
  );
}

export default CreateHome;
