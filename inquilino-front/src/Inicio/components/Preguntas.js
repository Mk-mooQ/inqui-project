// import { useRef, useState } from "react";
// import { Link } from "react-router-dom";
// import "./preguntas.css";
// import gif from "./gif/dudas1.gif";
// import duda1 from "./gif/duda1.gif";
// import duda2 from "./gif/duda2.gif";
// import duda3 from "./gif/duda3.gif";

// const PreguntasFrecuentes = () => {
//   return (
//     <div
//       className="formulario"
//       style={{
//         marginTop: "50px",
//       }}
//     >
//       <img src={gif} className="icono2" alt="Canvas Logo" />
//       <div
//         style={{
//           display: "flex",
//           flexDirection: "row",
//           alignItems: "center",
//           justifyContent: "center",
//           background: "white",
//           height: "100%",
//         }}
//       >
//         <img className="duda" src={duda1} alt="Canvas Logo" />
//         <img className="duda" src={duda2} alt="Canvas Logo" />
//         <img className="duda" src={duda3} alt="Canvas Logo" />
//       </div>

//       <h2
//         style={{
//           marginBottom: "30px",
//           paddingBlock: "30px",
//         }}
//       >
//         <Link to="/">Volver</Link>
//       </h2>
//     </div>
//   );
// };

// export { PreguntasFrecuentes };

import { useRef, useState } from "react";
import { Link } from "react-router-dom";
import "./preguntas.css";
import gif from "./gif/dudas1.gif";
import duda1 from "./gif/duda1.gif";
import duda2 from "./gif/duda2.gif";
import duda3 from "./gif/duda3.gif";

const PreguntasFrecuentes = () => {
  return (
    <div
      className="formulario"
      style={{
        marginTop: "50px",
      }}
    >
      <img src={gif} className="icono2" alt="Canvas Logo" />
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          textAlign: "center",
          justifyContent: "center",
          alignItems: "center",
          width: "30%",
        }}
      >
        <Link to="/preguntas1">
          <img className="duda" src={duda1} alt="Canvas Logo" />
        </Link>

        <Link to="/">
          <img
            className="duda"
            style={{
              marginRight: "10px",
              marginLeft: "10px",
            }}
            src={duda2}
            alt="Canvas Logo"
          />
        </Link>
        <Link to="/">
          <img className="duda" src={duda3} alt="Canvas Logo" />
        </Link>
      </div>

      <h2
        style={{
          marginBottom: "30px",
          paddingBlock: "30px",
        }}
      >
        <Link to="/">Volver</Link>
      </h2>
    </div>
  );
};

export { PreguntasFrecuentes };
