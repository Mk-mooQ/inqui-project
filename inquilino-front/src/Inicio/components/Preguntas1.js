import { useRef, useState } from "react";
import { Link } from "react-router-dom";
import "./preguntas.css";

const Preguntas1 = () => {
  return (
    <div
      className="formulario"
      style={{
        marginTop: "50px",
      }}
    >
      <div className="formulario">
        <h2>¿Qué pasa si no consigo darme de alta?</h2>
        <p>
          No pasa nada. Comunica con nosotros a través de email inqui@inqui.com
        </p>
      </div>
      <div className="formulario">
        <h2>No consigo acordarme de mi contraseña</h2>
        <p>
          Vale. Accede al enlace de restablecer contraseña. Te enviarémos una
          nueva
        </p>
      </div>
      <div className="formulario">
        <h2>No me acuerdo quién soy ni cómo me llamo</h2>
        <p>
          Uff. El problema parece grave. Mejor consulta a tu médico de cabecera
        </p>
      </div>

      <Link to="/">Volver</Link>
    </div>
  );
};

export { Preguntas1 };
