import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import useFetch from "../useFetch";
import "./Home.css";

function FirstSearch() {
  const [cityUrl, setCityUrl] = useState("");
  const [fechaEntrada, setFechaEntrada] = useState();
  const [fechaSalida, setFechaSalida] = useState();

  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();

    let newQueryUrl = "/searchPage?";

    if (cityUrl) {
      newQueryUrl += `ciudad=${cityUrl}`;
    }
    if (fechaEntrada) {
      newQueryUrl += `fecha_entrada=${fechaEntrada}`;
    }
    if (fechaSalida) {
      newQueryUrl += `fecha_salida=${fechaSalida}`;
    }

    history.push(newQueryUrl);
  };

  return (
    <div className="formulario">
      <label
        style={{
          fontFamily: "lobster_twoitalic , sans-serif",
          fontSize: "25px",
        }}
      >
        Busca una ciudad
        <form onSubmit={handleSubmit} className="firstSearchForm">
          <input
            type="text"
            placeholder="¿En qué ciudad buscas?"
            value={cityUrl}
            onChange={(e) => setCityUrl(e.target.value)}
            required
          />
          <div className="firstDatesContainer">
            <label className="searchDates">
              Entrada
              <input
                type="date"
                value={fechaEntrada}
                onChange={(e) => setFechaEntrada(e.target.value)}
              />
            </label>
            <label className="searchDates">
              Salida
              <input
                type="date"
                value={fechaSalida}
                onChange={(e) => setFechaSalida(e.target.value)}
              />
            </label>
          </div>
          <button>Buscar</button>
        </form>
      </label>
    </div>
  );
}

export default FirstSearch;
