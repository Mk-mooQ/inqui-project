import ErrorBoundary from "../ErrorBoundary";
import "./Home.css";
import { Link } from "react-router-dom";
import FirstSearch from "./FirstSearch";
import inquilino from "./components/inquilino.gif";
import portada from "./components/portada.jpg";
import casero from "./components/casero.gif";
import banner from "./components/banner.gif";
import banner2 from "./components/banner2.gif";
import "./components/Footer/footer.css";
import instagram from "./components/Footer/iconsFooter/instagram.png";
import linkedin from "./components/Footer/iconsFooter/linkedin.png";
import facebook from "./components/Footer/iconsFooter/facebook.png";
import twitter from "./components/Footer/iconsFooter/twitter.png";

import "./Home.css";

function Home() {
  return (
    <div className="pageHome">
      <div className="firstSearch">
        <img
          src={portada}
          style={{
            width: "100%",
          }}
          alt="Casa alquiler"
        />
        <h1 id="titulo">Encuentra tu casa ideal</h1>
        <ErrorBoundary>
          <FirstSearch />
        </ErrorBoundary>
        <div>
          <img
            src={inquilino}
            style={{
              width: "100%",
            }}
            alt="Casa alquiler"
          />
          <h1>Nuestros inquilinos</h1>
          <h2>
            Alquilar casa, piso o apartamento es fácil. Sólo tienes que darte de
            alta y empezar a seleccionar tu mejor opción.
          </h2>
        </div>
        <div>
          <img
            src={casero}
            style={{
              width: "100%",
            }}
            alt="Casa alquiler"
          />
          <h1>Nuestros caseros</h1>
          <h2>
            Date de alta y empieza a alquilar desde hoy. Podras ver las
            valaronaciones de tu inquilino antes de alquilar tu vivienda.
          </h2>
          <h2>¿Tienes alguna duda o pregunta?</h2>
          <button
            className="formulario"
            style={{
              fontSize: "20px",
            }}
          >
            <Link to="/preguntas">Ver más </Link>
          </button>
        </div>
        <div>
          <img
            src={banner}
            style={{
              marginTop: "30px",
              width: "100%",
            }}
            alt="banner"
          />
          <img
            src={banner2}
            style={{
              marginTop: "30px",
              width: "100%",
            }}
            alt="banner"
          />
          <h1>!Síguenos en redes sociales!</h1>
        </div>

        <div className="footerContainer">
          <div>
            <a href="https://www.facebook.com/">
              <img src={facebook} className="iconoYou" alt="Canvas Logo" />
            </a>
            <a href="https://www.instagram.com/">
              <img src={instagram} className="iconoYou" alt="Canvas Logo" />
            </a>
            <a href="https://www.twitter.com/">
              <img src={twitter} className="iconoYou" alt="Canvas Logo" />
            </a>
            <a href="https://www.linkedin.com/">
              <img src={linkedin} className="iconoYou" alt="Canvas Logo" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
