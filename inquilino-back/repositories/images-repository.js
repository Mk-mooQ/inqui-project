const { image } = require("faker");

const { getConnection } = require("../db/db");
const performQuery = async (query, params) => {
  let connection;
  console.log(query);
  console.log(params);

  try {
    connection = await getConnection();

    const [result] = await connection.query(query, params);

    return result;
  } catch (e) {
    console.log(e);
    throw new Error("database-error");
  } finally {
    if (connection) {
      connection.release();
    }
  }
};

const saveUserImage = async (imagen, id) => {
  const query = `update usuario set imagen = ? where id = ?`;
  const params = [imagen, id];

  console.log(query, params);
  await performQuery(query, params);
};

const saveHomeImage = async (imagen, id) => {
  const query = `update piso set imagen = ? where id = ?`;
  const params = [imagen, id];

  console.log(query, params);
  await performQuery(query, params);
};

module.exports = {
  saveUserImage,
  saveHomeImage,
};
