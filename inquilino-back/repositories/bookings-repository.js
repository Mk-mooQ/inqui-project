const { getConnection } = require("../db/db");
const performQuery = async (query, params) => {
  let connection;
  console.log(query);
  console.log(params);

  try {
    connection = await getConnection();

    console.log("performquery");

    const [result] = await connection.query(query, params);

    return result;
  } catch (e) {
    console.log(e);
    throw new Error("database-error");
  } finally {
    if (connection) {
      connection.release();
    }
  }
};

const createBooking = async (
  id_piso,
  id_usuario,
  fecha_entrada,
  fecha_salida
) => {
  const query = `insert into reserva(id_piso, id_usuario, fecha_entrada, fecha_salida, precio) values(?,?,?,?,(select precio_piso from piso where id = ?))`;
  const params = [id_piso, id_usuario, fecha_entrada, fecha_salida, id_piso];

  const result = await performQuery(query, params);
  return result;
};

const getBookingById = async (id) => {
  console.log("getbookingByIId");
  const query = `select r.id_reserva,
                      r.id_usuario,
                      p.id 'id_piso',
                      p.direccion,
                      p.ciudad,
                      r.precio,
                      r.fecha_entrada,
                      r.fecha_salida,
                      r.score_piso,
                      r.estado,
                      i.imagen,
                      r.score_usuario
                      from reserva r join piso p on r.id_piso =p.id left join imagenesPiso i on p.id=i.id_piso where r.id_reserva = ?`;
  const params = [id];

  const [result] = await performQuery(query, params);
  return result;
};

const getEmailBooking = async (resultId) => {
  const query = `select u.email
                      from usuario u
                      left join piso p on p.id_usuario = u.id
                      right join reserva r on p.id = r.id_piso where r.id_reserva = ? group by r.id_reserva`;
  const params = [resultId];

  const [result] = await performQuery(query, params);
  return result;
};

const homeBookings = async (id) => {
  const query = `select u.nombre 'nombre',
                      u.email 'email',
                      r.id_reserva 'id',
                      r.precio 'precio',
                      r.fecha_reserva 'fecha_reserva',
                      r.fecha_entrada 'fecha_entrada',
                      r.estado 'estado',
                      avg(r.score_usuario) "avg_scoreUsuario",
                      count (r.score_usuario) as countScoreUsuario,
                      r.fecha_salida 'fecha_salida' from reserva r join usuario u on r.id_usuario = u.id where id_piso = ? group by r.id_reserva`;
  const params = [id];

  const [...result] = await performQuery(query, params);
  return result;
};

const existBooking = async (fecha_entrada, fecha_salida, id) => {
  const query = `select * from reserva where (fecha_entrada between ? and ?
      and fecha_salida between ? and ?) and id_piso = ?`;
  const params = [fecha_entrada, fecha_salida, fecha_entrada, fecha_salida, id];

  const [result] = await performQuery(query, params);
  return result;
};

const deleteBooking = async (id) => {
  const query = `delete from reserva where id_reserva = ?`;
  const params = [id];

  await performQuery(query, params);
};
/**
 * Hay que poner un UPDATE, para que no se borren las cosas
 */

const getListBookings = async (id_usuario) => {
  const query = `select r.id_reserva,
                  r.id_usuario,
                  p.id 'id_piso',
                  p.direccion,
                  p.ciudad,
                  i.imagen,
                  r.precio,
                  r.fecha_entrada,
                  r.fecha_salida,
                  r.estado
                  from reserva r join piso p on r.id_piso =p.id left join imagenesPiso i on p.id=i.id_piso where r.id_usuario = ?`;
  const params = [id_usuario];

  const [...result] = await performQuery(query, params);
  return result;
};

const haveBooking = async (id_reserva) => {
  const query = `select
              r.id_reserva "reserva",
              r.id_piso "piso",
              r.id_usuario "inquilino",
              p.id_usuario "propietario"
          from reserva r
          join piso p on p.id = r.id_piso
          where r.id_reserva = ?
          group by r.id_reserva`;
  const params = [id_reserva];

  const [...result] = await performQuery(query, params);
  return result;
};

const acceptBooking = async (id_reserva) => {
  const query = `update reserva
                  set estado = 'aceptado'
                  where id_reserva = ?`;
  const params = [id_reserva];

  await performQuery(query, params);
};

const declineBooking = async (id_reserva) => {
  const query = `update reserva
                  set estado = 'declinado'
                  where id_reserva = ?`;
  const params = [id_reserva];

  await performQuery(query, params);
};

const scoreUser = async (score, id_reserva) => {
  const query = `update reserva set score_usuario = ? where id_reserva = ?`;
  const params = [score, id_reserva];

  await performQuery(query, params);
};

const scoreHome = async (score, id_reserva) => {
  const query = `update reserva set score_piso = ? where id_reserva = ?`;
  const params = [score, id_reserva];

  await performQuery(query, params);
};

module.exports = {
  createBooking,
  getBookingById,
  getEmailBooking,
  homeBookings,
  existBooking,
  deleteBooking,
  getListBookings,
  haveBooking,
  acceptBooking,
  declineBooking,
  scoreUser,
  scoreHome,
};
