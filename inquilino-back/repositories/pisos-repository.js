const { getConnection } = require("../db/db");
const performQuery = async (query, params) => {
  let connection;
  console.log(query);
  console.log(params);

  try {
    connection = await getConnection();

    const [result] = await connection.query(query, params);

    return result;
  } catch (e) {
    console.log(e);
    throw new Error("database-error");
  } finally {
    if (connection) {
      connection.release();
    }
  }
};

const createHome = async (
  fechaPublicacion,
  provincia,
  ciudad,
  direccion,
  precio_piso,
  nBanos,
  nHabitaciones,
  m2,
  descripcion,
  id_usuario
) => {
  const query = `insert into piso(fechaPublicacion, provincia, ciudad, direccion, precio_piso, nBanos, nHabitaciones, m2, descripcion, id_usuario)
      VALUES(UTC_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
  const params = [
    provincia,
    ciudad,
    direccion,
    precio_piso,
    nBanos,
    nHabitaciones,
    m2,
    descripcion,
    id_usuario,
  ];
  const result = await performQuery(query, params);
  return result;
};

const listPisos = async () => {
  const query = `select * from piso`;
  const [...result] = await performQuery(query);
  return result;
};

const myHomes = async (id) => {
  const query = `select * from piso where id_usuario = ?`;
  const params = [id];

  const [...result] = await performQuery(query, params);
  return result;
};

// p.imagen 'imagen',
// p.score 'piso_score',
const getHome = async (id) => {
  const query = `select p.provincia "provincia",
                  p.ciudad "ciudad",
                  p.direccion "direccion",
                  p.nBanos "nBanos",
                  p.nHabitaciones "nHabitaciones",
                  p.m2 "m2",
                  p.precio_piso "precio_piso",
                  p.id_usuario "id_usuario",
                  u.nombre 'nombre',
                  avg(r.score_piso) "avg_score",
                  count (score_piso) as countScore
      from piso p left join reserva r on p.id = r.id_piso right join usuario u on p.id_usuario= u.id where p.id =  ? group by p.id`;
  const params = [id];

  const result = await performQuery(query, params);
  return result;
};

const getOwner = async (id_piso) => {
  const query = `select p.id "id_piso",
      u.id "owner_id",
      u.email "email" from piso p join usuario u on p.id_usuario = u.id where p.id= ?`;
  const params = [id_piso];

  const [...result] = await performQuery(query, params);
  return result;
};

const deleteHome = async (id) => {
  const query = `delete from piso where id = ?`;
  const params = [id];

  await performQuery(query, params);
};

const updateHome = async (
  provincia,
  ciudad,
  direccion,
  precio_piso,
  nBanos,
  nHabitaciones,
  m2,
  descripcion,
  id_usuario,
  id
) => {
  const query = `
      update piso set fechaActualizacion = UTC_TIMESTAMP,
      provincia = ?,
      ciudad = ?,
      direccion = ?,
      precio_piso = ?,
      nBanos = ?,
      nHabitaciones = ?,
      m2 = ?,
      descripcion = ?,
      id_usuario = ?
      where id = ?`;

  const params = [
    provincia,
    ciudad,
    direccion,
    precio_piso,
    nBanos,
    nHabitaciones,
    m2,
    descripcion,
    id_usuario,
    id,
  ];
  await performQuery(query, params);
};

async function filterPisos(queries) {
  const {
    provincia,
    ciudad,
    nBanos,
    nHabitaciones,
    m2,
    precio1,
    precio2,
    fecha_entrada,
    fecha_salida,
    order,
    direction,
  } = queries;

  const provinciaQuery = `%${provincia || ""}%`;
  const ciudadQuery = `%${ciudad || ""}%`;

  const address = `%${direction || ""}%`;
  const orderQuery = order || "desc";

  console.log(order);

  const consulta = `SELECT * FROM piso WHERE (? is null or nBanos=?) and (? is null or nHabitaciones=?) and m2 > if (? is not null, ?, 0) and precio_piso between if (? is not null, ?, 0) and if (? is not null, ?, 50000) and direccion like ? and ciudad like ? and provincia like ? order by precio_piso ${orderQuery}`;

  const params = [
    nBanos,
    nBanos,
    nHabitaciones,
    nHabitaciones,
    m2,
    m2,
    precio1,
    precio1,
    precio2,
    precio2,
    address,
    ciudadQuery,
    provinciaQuery,
  ];

  const pisos = await performQuery(consulta, params);

  return pisos;
}

module.exports = {
  createHome,
  filterPisos,
  getHome,
  getOwner,
  listPisos,
  myHomes,
  deleteHome,
  updateHome,
};
