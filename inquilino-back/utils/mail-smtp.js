const nodemailer = require("nodemailer");
const {
  HTTP_SERVER_DOMAIN,
  SMTP_PORT,
  SMTP_HOST,
  SMTP_USER,
  SMTP_PASS,
  SMTP_FROM,
} = process.env;
const transporter = nodemailer.createTransport({
  port: SMTP_PORT,
  host: SMTP_HOST,
  auth: { user: SMTP_USER, pass: SMTP_PASS },
  secure: false,
});
async function sendBooking(ownerEmail, id) {
  const mailData = {
    from: SMTP_FROM,
    to: ownerEmail,
    subject: "Tienes una nueva petición de reserva",
    html: `<p>Hola! Tienes una nueva reserva. Acepta o rechaza aquí abajo! </p> <a href="http://localhost:3000/reserva/${id}/accept">Aceptar</a> <a href="http://localhost:3000/reserva/${id}/decline">Rechazar</a>`,
  };
  const data = await transporter.sendMail(mailData);
  return data;
}

module.exports = { sendBooking };
