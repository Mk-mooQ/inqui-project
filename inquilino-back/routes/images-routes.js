const express = require("express");
const router = express.Router();

const {
  saveImage,
  saveHomeImage,
  getImage,
} = require("../controllers/images/images");

// Endpoint públicos
router.route("/").post(saveImage);

module.exports = router;
