const express = require("express");
const router = express.Router();

const { addPiso } = require("../controllers/pisos/create-piso");
const { getListOfPisos } = require("../controllers/pisos/get-pisos");
const { getPiso } = require("../controllers/pisos/get-piso-by-id");
const { deletePiso } = require("../controllers/pisos/delete-piso");
const { updatePiso } = require("../controllers/pisos/update-piso");
const { createBooking } = require("../controllers/bookings/create-booking");
const {
  homeBookings,
} = require("../controllers/bookings/get-booking-by-piso-id");
const { searchPisos } = require("../controllers/pisos/search-pisos");

const { isAuthenticated, isOwner } = require("../middlewares/auth");

const { saveHomeImage, getImage } = require("../controllers/images/images");

// Endpoint públicos
router.route("/").get(getListOfPisos);
router.route("/search").get(searchPisos);
router.route("/imagen/:id").post(saveHomeImage);
router.route("/imagen/:uuid").get(getImage);
router.route("/:id").get(getPiso);

// Endpoint privados
router.route("/").all(isAuthenticated).post(addPiso);
router
  .route("/:id")
  .all(isAuthenticated, isOwner)
  .delete(deletePiso)
  .put(updatePiso);
router.route("/imagen/:id").post(saveHomeImage);
router.route("/:id/reserva").all(isAuthenticated).post(createBooking);
router.route("/reservas/:id").all(isAuthenticated, isOwner).get(homeBookings);

module.exports = router;
