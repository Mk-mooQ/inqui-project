const express = require("express");
const router = express.Router();

const {
  getListOfBookings,
} = require("../controllers/bookings/get-booking-by-user-id");
const { getBooking } = require("../controllers/bookings/get-booking");
const { acceptBooking } = require("../controllers/bookings/accept-booking");
const { declineBooking } = require("../controllers/bookings/decline-booking");
const { deleteBooking } = require("../controllers/bookings/delete-booking");

const {
  isAuthenticated,
  isSameUserOrAdmin,
  haveBooking,
} = require("../middlewares/auth");

// Endpoint privados

router
  .route("/")
  .all(isAuthenticated, isSameUserOrAdmin)
  .get(getListOfBookings);

router.route("/:id").all(haveBooking).get(getBooking);
router.route("/:id").all(isAuthenticated).delete(deleteBooking);

router
  .route("/accept/:id")
  .all(isAuthenticated, haveBooking)
  .put(acceptBooking);

router
  .route("/decline/:id")
  .all(isAuthenticated, haveBooking)
  .put(declineBooking);

module.exports = router;
