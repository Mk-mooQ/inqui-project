const express = require("express");
const router = express.Router();

const { search } = require("../controllers/buscador/search");

// Endpoint públicos
router.route("/").get(search);

module.exports = router;
