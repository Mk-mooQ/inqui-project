const express = require("express");
const router = express.Router();

const { register } = require("../controllers/users/register");
const { login } = require("../controllers/users/login");
const { validate } = require("../controllers/users/validate-user");
const { getMyHomes } = require("../controllers/pisos/get-pisos-by-user-id");
const { deleteUser } = require("../controllers/users/delete-user");
const { getUserById } = require("../controllers/users/get-user-by-id");
const { updateUser } = require("../controllers/users/update-user");
const { updateUserEmail } = require("../controllers/users/update-user-email");
const {
  updateUserPassword,
} = require("../controllers/users/update-user-password");
const { recoverPassword } = require("../controllers/users/recover-password");
const { resetPassword } = require("../controllers/users/reset-password");

const { isAuthenticated, isSameUserOrAdmin } = require("../middlewares/auth");

// Endpoint públicos
router.route("/").post(register);
router.route("/login").post(login);
router.route("/validate/:code").get(validate);
router.route("/:id").get(getUserById);
router.route("/piso/:id").get(getMyHomes);
router.route("/recover-password").post(recoverPassword);
router.route("/password/reset/:code").put(resetPassword);

// Endpoint privados
router
  .route("/:id")
  .all(isAuthenticated, isSameUserOrAdmin)
  .delete(deleteUser)
  .put(updateUser);
router
  .route("/:id/password")
  .all(isAuthenticated, isSameUserOrAdmin)
  .put(updateUserPassword);
router
  .route("/:id/email")
  .all(isAuthenticated, isSameUserOrAdmin)
  .put(updateUserEmail);

module.exports = router;
