const db = require("../../repositories/images-repositories");

const fsPromises = require("fs").promises;
const uuid = require("uuid");

const getImage = async (req, res) => {
  const { uuid } = req.params;

  const imagePath =
    "/home/andres/Escritorio/proyectoFinal/Inquilino-Perfecto/images";

  const path = `${imagePath}/${uuid}.jpg`;
  console.log(path);

  try {
    await fsPromises.stat(path);
    res.sendFile(path);
  } catch (e) {
    console.log("El fichero no existe");
    res.status(404).send();
  }
};

module.exports = {
  getImage,
};
