const db = require("../../repositories/pisos-repository");

const deletePiso = async (req, res) => {
  const { id } = req.params;

  try {
    const home = await db.getPiso(id);

    if (!home.length) {
      res.status(404).send();
      return;
    }

    await db.deletePiso(id);

    res.send();
  } catch (e) {
    if (e.message === "unknown-id") {
      res.status(404).send();
    } else {
      res.status(500).send();
    }
  }
};

module.exports = {
  deletePiso,
};
