const db = require("../../repositories/pisos-repository");

const getListOfPisos = async (req, res) => {
  try {
    let homes = await db.listPisos();
    res.send(homes);
  } catch (e) {
    res.status(500).send();
  }
};

module.exports = {
  getListOfPisos,
};
