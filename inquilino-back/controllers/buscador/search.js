const { getConnection } = require("../../db/db");
const { dateToDb } = require("../../utils/moment");
const search = async (req, res, next) => {
  let connection;
  try {
    connection = await getConnection();
    const {
      provincia,
      ciudad,
      nBanos,
      nHabitaciones,
      m2,
      precio1,
      precio2,
      fecha_entrada,
      fecha_salida,
      order,
      direction,
    } = req.query;
    let query = `select piso.id, piso.precio, avg(reserva.score_piso) 'avg_score', avg(reserva.score_usuario) 'score_usuario', count (score_piso) as countScores, piso.provincia, piso.ciudad, piso.m2, piso.direccion from piso
                    left outer join reserva on reserva.id_piso = piso.id`;
    const params = [];
    const orderDirection =
      (direction && direction.toLowerCase()) === "asc" ? "ASC" : "DESC";
    let orderBy;
    switch (order) {
      case "precio":
        orderBy = "precio";
        break;
      case "fechaActualizacion":
        orderBy = "fechaActualizacion";
        break;
      default:
        orderBy = "fechaActualizacion";
    }
    if (
      provincia ||
      ciudad ||
      nBanos ||
      nHabitaciones ||
      m2 ||
      precio1 ||
      precio2 ||
      (fecha_entrada && fecha_salida)
    ) {
      //ESTABLECEMOS CONDICIONES PARA LA QUERY
      const conditions = [];
      if (provincia) {
        conditions.push(`provincia LIKE ?`);
        params.push(`%${provincia}%`);
      }
      if (ciudad) {
        conditions.push(`ciudad LIKE ?`);
        params.push(`%${ciudad}%`);
      }
      if (nBanos) {
        conditions.push(`nBanos >= ?`);
        params.push(`${nBanos}`);
      }
      if (nHabitaciones) {
        conditions.push(`nHabitaciones >= ?`);
        params.push(`${nHabitaciones}`);
      }
      if (m2) {
        conditions.push(`m2 >= ?`);
        params.push(`${m2}`);
      }
      if (precio1 || precio2) {
        if (!precio1) {
          conditions.push(`precio <= ?`);
          params.push(`${precio2}`);
        } else if (!precio2) {
          conditions.push(`precio >= ?`);
          params.push(`${precio1}`);
        } else if (precio1 && precio2) {
          conditions.push(`precio between ? and ?`);
          params.push(`${precio1}`, `${precio2}`);
        }
      }
      if (fecha_entrada && fecha_salida) {
        const entrada = dateToDb(fecha_entrada);
        const salida = dateToDb(fecha_salida);
        conditions.push(`(fecha_entrada not between ? and ?
                    and fecha_salida not between ? and ?
                    and not (fecha_entrada < ? and fecha_salida > ?)
                    or (reserva.id_reserva is null))`);
        params.push(
          `${entrada}`,
          `${salida}`,
          `${entrada}`,
          `${salida}`,
          `${entrada}`,
          `${salida}`
        );
      }
      query = `${query} where ${conditions.join(
        ` and `
      )} group by piso.id order by ${orderBy} ${orderDirection}`;
    }
    console.log(query, params);
    const [result] = await connection.query(query, params);
    res.send({
      data: result,
    });
  } catch (e) {
    next(e);
  } finally {
    if (connection) {
      connection.release();
    }
  }
};
module.exports = {
  search,
};
