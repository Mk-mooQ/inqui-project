const db = require("../../repositories/users-repository");

const deleteUser = async (req, res) => {
  const { id } = req.params;
  try {
    const user = await db.getUserById(id);
    if (!user) {
      res.status(404).send();
      return;
    }
    await db.deleteUserById(id);
    res.send();
  } catch (e) {
    if (e.message === "unknown-id") {
      res.status(404).send();
    } else {
      res.status(500).send();
    }
  }
};
module.exports = {
  deleteUser,
};
