const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const randomstring = require("randomstring");

const db = require("../../repositories/users-repository");

const login = async (req, res) => {
  const { email, password } = req.body;

  const user = await db.getUser(email);

  if (!user) {
    res.status(401).send();
    return;
  }

  const username = user.nombre;
  const imagen = user.imagen;
  const userEmail = user.email;
  const id = user.id;

  const passwordIsvalid = await bcrypt.compare(password, user.password);

  if (!passwordIsvalid) {
    res.status(401).send();
    return;
  }

  const tokenPayload = {
    isAdmin: user.role === "admin",
    role: user.role,
    email: user.email,
    id: user.id,
    /**
     * Ponemos cualquier tipo de información que
     * pueda ser de utilidad en los endpoints
     */
  };

  const token = jwt.sign(tokenPayload, process.env.SECRET, {
    expiresIn: "1d",
  });

  res.send({
    token,
    userEmail,
    username,
    imagen,
    id,
  });
};

module.exports = {
  login,
};
