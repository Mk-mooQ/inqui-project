const db = require("../../repositories/users-repository");
const fsPromises = require("fs").promises;
const uuid = require("uuid");

const updateUser = async (req, res) => {
  const {
    nombre,
    apellidos,
    fechaNacimiento,
    provincia,
    ciudad,
    descripcion,
    telefono,
  } = req.body;
  const { id } = req.params;

  if (req.files) {
    await fsPromises.mkdir(process.env.TARGET_FOLDER, { recursive: true });

    try {
      const fileID = uuid.v4();
      const outputFileName = `${process.env.TARGET_FOLDER}/${fileID}.jpg`;

      await fsPromises.writeFile(outputFileName, req.files.imagen.data);

      await db.saveUserImage(fileID, id);
      res.send();
    } catch (e) {
      console.log("Error: ", e);
      res.status(500).send();
    }
  }

  try {
    await db.updateUser(
      nombre,
      apellidos,
      fechaNacimiento,
      provincia,
      ciudad,
      descripcion,
      telefono,
      id
    );
  } catch (e) {
    console.log(e);
    let statusCode = 400;
    if (e.message === "database-error") {
      statusCode = 500;
    }
    res.status(statusCode).send(e.message);
    return;
  }
  res.send();
};
/**
 * Hay que hacer algo con los notnull, ya que los piden para actualizar los
 * cambios. IFNULL? ISNULL? SQL.
 */
module.exports = {
  updateUser,
};
