const db = require("../../repositories/users-repository");

const validate = async (req, res) => {
  const { code } = req.params;

  try {
    db.checkValidationCode(code);
    res.send("Validado correctamente");
  } catch (e) {
    res.status(401).send("Usuario no validado");
  }
};

module.exports = {
  validate,
};
