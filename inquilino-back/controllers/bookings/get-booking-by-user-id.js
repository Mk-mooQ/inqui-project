const jwt = require("jsonwebtoken");
const db = require("../../repositories/bookings-repository");
const dbUser = require("../../repositories/users-repository");

const getListOfBookings = async (req, res) => {
  console.log("holaaaa");
  const { authorization } = req.headers;

  try {
    const decodedToken = jwt.verify(authorization, process.env.SECRET);
    const id_usuario = await dbUser.getUser(decodedToken.email);
    let bookings = await db.getListBookings(id_usuario.id);
    res.send(bookings);
  } catch (e) {
    console.log(e);
    res.status(403).send();
  }
};
module.exports = {
  getListOfBookings,
};
