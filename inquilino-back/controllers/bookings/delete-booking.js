const db = require("../../repositories/bookings-repository");

const deleteBooking = async (req, res) => {
  const { id } = req.params;

  try {
    const booking = await db.getBooking(id);

    if (!booking) {
      res.status(404).send();
      return;
    }

    await db.deleteBooking(id);
  } catch (e) {
    console.log(e);
    if (e.message === "unknown-id") {
      res.status(404).send();
    } else {
      console.log(e);
      res.status(500).send();
    }
  }
  res.send();
};
module.exports = {
  deleteBooking,
};
