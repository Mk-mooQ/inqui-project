const db = require("../../repositories/bookings-repository");

const declineBooking = async (req, res) => {
  const { id } = req.params;

  try {
    await db.declineBooking(id);
    res.send();
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
};
module.exports = {
  declineBooking,
};
