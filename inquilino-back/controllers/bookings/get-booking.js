const db = require("../../repositories/bookings-repository");

const getBooking = async (req, res) => {
  const { id } = req.params;

  try {
    console.log("getbooking");
    const booking = await db.getBookingById(id);

    if (!booking) {
      console.log("Es este ");
      res.status(404).send();
    } else {
      res.send(booking);
    }
  } catch (e) {
    res.status(500).send();
  }
};
module.exports = {
  getBooking,
};
