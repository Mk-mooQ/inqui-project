const db = require("../../repositories/bookings-repository");

const acceptBooking = async (req, res) => {
  const { id } = req.params;

  console.log(id);

  try {
    await db.acceptBooking(id);
    res.send();
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
};
module.exports = {
  acceptBooking,
};
