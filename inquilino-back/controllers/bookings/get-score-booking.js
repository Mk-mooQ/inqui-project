const db = require("../../repositories/booking-repositories");

const scoreBooking = async (req, res) => {
  const { score } = req.body;
  const { authorization } = req.headers;

  console.log(score);

  try {
    // await scoreValidator.validateAsync(req.body)

    const bookingId = parseInt(req.params.id);

    const decodedToken = jwt.verify(authorization, process.env.SECRET);
    const { id } = await db.getUser(decodedToken.email);

    const { id_usuario } = await db.getBooking(bookingId);
    const propietario = await db.haveBooking(bookingId);
    const id_propietario = propietario[0].propietario;

    if (id === id_usuario) {
      await db.scoreHome(score, bookingId);
      console.log("Ha puntuado al piso");
    } else if (id === id_propietario) {
      await db.scoreUser(score, bookingId);
      console.log("Ha puntuado al usuario");
    } else {
      console.log("Da error aquí");
      res.status(403).send();
    }
  } catch (e) {
    console.log(e);
    res.status(403).send();
    return;
  }
  res.send();
};
module.exports = {
  scoreBooking,
};
