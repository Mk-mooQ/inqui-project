const db = require("../../repositories/bookings-repository");
const dbUser = require("../../repositories/users-repository");
const jwt = require("jsonwebtoken");
const { scoreValidator } = require("../../validators/score");
const { sendBooking } = require("../../utils/mail-smtp");

// const utils = require("../../utils/utils");

const createBooking = async (req, res) => {
  const { id } = req.params;

  const { fecha_entrada, fecha_salida } = req.body;

  const { authorization } = req.headers;

  const booking = await db.existBooking(fecha_entrada, fecha_salida, id);

  if (booking) {
    res.status(403).send();
    return;
  }

  try {
    const decodedToken = jwt.verify(authorization, process.env.SECRET);
    const id_usuario = await dbUser.getUser(decodedToken.email);

    const result = await db.createBooking(
      id,
      id_usuario.id,
      fecha_entrada,
      fecha_salida
    );
    const resultId = result.insertId;

    const { email } = await db.getEmailBooking(resultId);

    // utils.sendBookingMail(
    //   email,
    //   `http://${process.env.FRONT_DOMAIN}/myHome/${id}`
    // );
    console.log("email:", email);
    await sendBooking(email, resultId);

    res.send({ resultId });
  } catch (e) {
    console.log(e);
    res.status(402).send();
  }
};
module.exports = {
  createBooking,
};
