drop database inquilinoPerfecto;
create database if not exists inquilinoPerfecto character set="utf8mb4" collate="utf8mb4_spanish2_ci";

use inquilinoPerfecto;

create table if not exists usuario
(
  id int unsigned auto_increment primary key,
  nombre varchar(100),
  apellidos varchar(100),
  provincia varchar(100),
  ciudad varchar(100),
  email varchar(200) unique not null,
  password varchar(100) not null,
  validationCode varchar(100),
  estado enum('activo', 'inactivo') default 'inactivo' ,
  administrador boolean not null default false,
  descripcion text,
  fechaNacimiento date,
  imagen varchar(500),
  telefono varchar(20),
  role enum('admin', 'user') not null default 'user'
);

create table if not exists piso
(
  id int unsigned auto_increment primary key,
  provincia varchar(100) not null,
  ciudad varchar(100) not null,
  direccion varchar(200) not null,
  nBanos smallint unsigned not null,
  nHabitaciones smallint unsigned not null,
  m2 float not null,
  precio bigint unsigned not null,
  ascensor enum('si','no') not null default 'no',
  garaje enum('si', 'no') not null default 'no',
  balcon smallint default 0,
  jardin enum('si', 'no') not null default 'no' ,
  estado enum('activo','inactivo') not null default 'activo',
  fechaPublicacion timestamp default current_timestamp,
  fechaActualizacion timestamp default current_timestamp on update current_timestamp,
  id_usuario int unsigned not null,
  descripcion varchar(500),
  constraint piso_id_usuario_fk1
  foreign key (id_usuario) references usuario(id)
);

create table if not exists imagenesPiso
(
  id int unsigned auto_increment primary key,
  imagen varchar(500),
  id_piso int unsigned not null,
  constraint imagenes_piso_id_piso_fk1
  foreign key (id_piso) references piso(id)
);

create table if not exists reserva
(
  id_reserva int unsigned auto_increment primary key,
  precio bigint unsigned not null,
  fecha_entrada timestamp unique,
  fecha_salida timestamp unique,
  score_piso float not null default 0,
  score_usuario float not null default 0,
  estado enum('aceptado', 'declinado'),
  id_piso int unsigned not null,
  id_usuario int unsigned not null,
  constraint reserva_id_piso_fk1
  foreign key (id_piso) references piso(id),
  constraint reserva_id_usuario_fk2
  foreign key (id_usuario) references usuario(id)
);